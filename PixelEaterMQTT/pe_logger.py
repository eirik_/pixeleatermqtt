import sys
import paho.mqtt.client as mqtt
from pe_config import *
import json
import ssl
import time
import datetime

t0 = time.time()

client = None
command = None

log_file = "pe_log {}.txt".format(datetime.date.today())
date = datetime.date.today()
log_data = []


def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Cannot connect rc= {}".format(rc))
        if rc == 5:
            print("Not authorized")
        sys.exit(1)
    print("Connected with result code {}".format(str(rc)))
    sys.stdout.flush()


def on_message(client, userdata, msg):
    global command, log_data

    print(msg.topic, json.loads(bytes.decode(msg.payload)))
    log_data.append("Topic: {} | Payload: {}".format(msg.topic, json.loads(bytes.decode(msg.payload))))


def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed")


def init():
    global client

    client = mqtt.Client(client_id="logger")
    client.username_pw_set("logger", password="ADUIIjdiq624")
    client.tls_set(ca_certs=ca_cert, tls_version=ssl.PROTOCOL_TLSv1_2)

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_subscribe = on_subscribe

    try:
        client.connect(brokeraddress, 8883)
        client.loop_start()
        client.subscribe(starttopic, qos=1)
        client.subscribe(joysticktopic)

        return client

    except:
        print("Failed to connect")
        sys.exit(1)


if __name__ == "__main__":
    client = init()

    while True:
        time.sleep(0.1)
        if date != datetime.date.today():
            log_file = datetime.date.today()

        if time.time() > t0 + 10:
            print("Writing to file")
            try:
                with open(log_file, "a") as f:
                    f.writelines("\n".join(log_data))

                log_data.clear()
                t0 = time.time()

            except OSError:
                print("Problem opening or writing to file")
