import sys
import paho.mqtt.client as mqtt
from sense_emu import SenseHat
from pe_config import *
import json
import ssl
from time import sleep

sense = None
client = None
command = None
visited = None
was_stopped = False

x_pos = 0
y_pos = 0


def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Cannot connect rc= {}".format(rc))
        if rc == 5:
            print("Not authorized")
        sys.exit(1)
    print("Connected with result code {}".format(str(rc)))
    sys.stdout.flush()


def on_message(client, userdata, msg):
    global command, x_pos, y_pos
    if msg.topic == starttopic:
        try:
            start = json.loads(bytes.decode(msg.payload))
        except ValueError:
            print("Unexpected input: {}".format(str(msg.payload)))

        if start == ordered_cmd:
            command = start
            return

        if start == timed_cmd:
            pass
            return

        if start == stop_cmd:
            command = start
            return

        if start == exit_cmd:
            command = start
            return

        else:
            print("Unexpected string: {}".format(str(msg.payload)))

    if msg.topic == joysticktopic:
        try:
            event = json.loads(bytes.decode(msg.payload))
        except ValueError:
            print("Unexpected input: {}".format(str(msg.payload)))

        if (event[1] == "up" and event[2] == "pressed" or event[1] == "up" and event[2] == "held"):
            x_pos = (x_pos + 1) % 8
            visited[x_pos][y_pos] = True
            draw()

        if (event[1] == "down" and event[2] == "pressed" or event[1] == "down" and event[2] == "held"):
            x_pos = (x_pos - 1) % 8
            visited[x_pos][y_pos] = True
            draw()

        if (event[1] == "right" and event[2] == "pressed" or event[1] == "right" and event[2] == "held"):
            y_pos = (y_pos + 1) % 8
            visited[x_pos][y_pos] = True
            draw()

        if (event[1] == "left" and event[2] == "pressed" or event[1] == "left" and event[2] == "held"):
            y_pos = (y_pos - 1) % 8
            visited[x_pos][y_pos] = True
            draw()


def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed")


def init():
    global client, sense

    sense = SenseHat()
    sense.set_rotation(senshat_rotation)
    sense.clear()

    client = mqtt.Client(client_id="viewer")
    client.username_pw_set("viewer", password="FAJJKeyrt835")
    client.tls_set(ca_certs=ca_cert, tls_version=ssl.PROTOCOL_TLSv1_2)

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_subscribe = on_subscribe

    try:
        client.connect(brokeraddress, 8883)
        client.loop_start()
        client.subscribe(starttopic, qos=1)
        client.subscribe(joysticktopic)

        return client, sense

    except:
        print("Failed to connect")
        sys.exit(1)


def matrix_init():
    global x_pos, y_pos, visited

    visited = [[False for i in range(0,8)] for j in range(0,8)]
    x_pos = 0
    y_pos = 0
    sense.clear(white)
    sense.set_pixel(x_pos,y_pos,eater)
    visited[x_pos][y_pos] = True


def draw():
    global x_pos, y_pos, visited

    for x in range(0,8):
        for y in range(0,8):
            if visited[x][y] == True:
                sense.set_pixel(x, y, trail)
    sense.set_pixel(x_pos, y_pos, eater)

def allEaten():
    accum = 0
    for x in range(0,8):
        for y in range(0,8):
            if(visited[x][y] == True):
                accum += 1
    if(accum == 64):
        return True


if __name__ == "__main__":
    client, sense = init()
    matrix_init()

    while True:
        sleep(0.1)
        if command == "ordered":
            if was_stopped == True:
                matrix_init()
                was_stopped = False

            if allEaten() == True:
                sense.show_message("Good job!")
                was_stopped = True
                client.publish(starttopic, json.dumps(stop_cmd), qos=1, retain=True)

        if command == "stop":
            was_stopped = True
            sense.clear()

        if command == "exit":
            print("Exit command received, exiting..")
            sense.show_message("Exiting..")
            sys.exit(1)