import sys
import paho.mqtt.client as mqtt
from sense_hat import SenseHat
from pe_config import *
import json
import ssl
from time import sleep

sense = None
client = None
joystick_event = None
command = None


def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Cannot connect rc= {}".format(rc))
        if rc == 5:
            print("Not authorized")
        sys.exit(1)
    print("Connected with result code {}".format(str(rc)))
    sys.stdout.flush()


def on_message(client, userdata, msg):
    global joystick_event, command

    if msg.topic == starttopic:
        try:
            start = json.loads(bytes.decode(msg.payload))
        except ValueError:
            print("Unexpected input: {}".format(str(msg.payload)))
        print(start)
        if start == ordered_cmd:
            command = start
            return

        if start == timed_cmd:
            pass
            return

        if start == stop_cmd:
            command = start
            return

        if start == exit_cmd:
            command = start
            return

        else:
            print("Unexpected string: {}".format(str(msg.payload)))


def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed")


def init():
    global client, sense

    sense = SenseHat()
    sense.set_rotation(senshat_rotation)
    sense.clear()

    client = mqtt.Client(client_id="player")
    client.username_pw_set("player", password="CDGRYbhge915")
    client.tls_set(ca_certs=ca_cert, tls_version=ssl.PROTOCOL_TLSv1_2)

    client.on_connect = on_connect
    client.on_message = on_message
    client.on_subscribe = on_subscribe

    try:
        client.connect(brokeraddress, 8883)
        client.loop_start()
        client.subscribe(starttopic, qos=1)
        client.subscribe(joysticktopic)
        client.publish(starttopic, json.dumps(stop_cmd), qos=1, retain=True)

        return client, sense

    except:
        print("Failed to connect")
        sys.exit(1)


if __name__ == "__main__":
    client, sense = init()

    while True:
        sleep(0.1)

        if command == ordered_cmd:
            for event in sense.stick.get_events():
                client.publish(joysticktopic, json.dumps(event))

        if command == stop_cmd:
            sense.clear()
            sense.show_message("Press middle for start, right for stop and left for exit")
            choice = sense.stick.wait_for_event()
            if choice.direction == "middle" and choice.action == "pressed":
                client.publish(starttopic, json.dumps(ordered_cmd), qos=1, retain=True)
                sense.show_message("Starting game")
            if choice.direction == "right" and choice.action == "pressed":
                client.publish(starttopic, json.dumps(stop_cmd), qos=1, retain=True)
                sense.show_message("Stop")
            if choice.direction == "left" and choice.action == "pressed":
                client.publish(starttopic, json.dumps(exit_cmd), qos=1, retain=True)

        if command == exit_cmd:
            print("Exit command received, exiting..")
            sense.show_message("Exiting..")
            sys.exit(1)
