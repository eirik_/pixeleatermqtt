'''
Config file
'''

#CA cert
ca_cert = "dat235ca.crt"

#Hosts
brokeraddress="10.0.0.182"

#  Topic
maintopic   = "dat235/group10/"
starttopic  = maintopic+"start"
joysticktopic = maintopic+"joystick"


# Commands
ordered_cmd = "ordered"
timed_cmd = "timed"
stop_cmd = "stop"
exit_cmd ="exit"

#SenseHat colors
red = (127,0,0)
green = (0,127,0)
blue = (0,0,127)
white = (127,127,127)
yellow = (127,127,0)
black = (0,0,0)
trail = black  # the eater trail
eater = (255,255,0)   # the eater position (bright yellow)


#SenseHat
senshat_rotation = 0
